'use strict';

const Datepicker =  angular.module('Datepicker', ['ngMaterial', 'ngMessages'])

Datepicker.controller('Datepicker', function($scope, $rootScope) {
	this.onTxtChanged = function() {
		$rootScope.dateFrom = $scope.dateFromIndicator;
  		$rootScope.dateTo = $scope.dateToIndicator;
  		$scope.$broadcast('txt')
	}
})
