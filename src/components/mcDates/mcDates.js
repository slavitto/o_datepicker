'use strict';

Datepicker
    .component('mcDates', {
        templateUrl: 'src/components/mcDates/mcDates.html'
    })
    .controller('mcDates', [ '$scope', '$rootScope', function($scope, $rootScope) {

    	this.dateFrom = new Date();
    	this.dateTo = new Date();
  		this.isOpen = false;

  		this.onDateChanged = function() {

  			if(this.dateTo < this.dateFrom) {

  				this.dateTo = this.dateFrom;
  				alert('вторая дата не может быть раньше первой!')

  			} else {

  				$rootScope.dateFromIndicator = this.dateFrom ? moment(this.dateFrom).format("YYYY-MM-DD") : '';
	      		$rootScope.dateToIndicator = this.dateTo ? moment(this.dateTo).format("YYYY-MM-DD") : '';
	      		alert('Даты изменены на ' + $rootScope.dateFromIndicator + ' и ' + $rootScope.dateToIndicator);

  			}

	    };

	    this.setInterval = function(interval) {
	    	switch(interval) {
	    		case -1:
	    		this.dateFrom = moment(new Date()).subtract(1, 'days');
	    		this.dateTo = this.dateFrom;
	    		break;
	    		case 1:
	    		this.dateFrom = new Date();
	    		this.dateTo = new Date();
	    		break;
	    		case -14:
	    		this.dateFrom = moment(new Date()).subtract(14, 'days');
	    		this.dateTo = new Date();
	    		break;
	    		case -30:
	    		this.dateFrom = moment(new Date()).subtract(30, 'days');
	    		this.dateTo = new Date();
	    		break;
	    		case 0:
	    		this.dateTo = null; 
	    		this.dateFrom = null;
	    	}
	    	this.onDateChanged()
	    }

	    $scope.$on('txt', function() {
	    	this.dateFrom = $rootScope.dateFrom;
	    	this.dateTo = $rootScope.dateTo;
	    }.bind(this))
    }])